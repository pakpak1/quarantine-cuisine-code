﻿using System.Collections;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(ARTrackedImageManager))]

public class ImageRecognition : MonoBehaviour
{

    public AudioSource Confirmation;
    public GameObject qrReaderConfirmed;
    public GameObject QRCodeARSO;
    public GameObject QRCodeUI;
    public GameObject MealMenuUI;
    public GameObject UIControl;
    public GameObject MainCamera;

    ARTrackedImageManager _arTrackedImageManager;

    public void PlayConfirmation()
    {
        Confirmation.Play();
    }

    public void QRConfirmation()
    {
        qrReaderConfirmed.GetComponent<Animator>().Play("Confirmed QR Code Scan");
    }

    void Awake()
    {
        _arTrackedImageManager = GetComponent<ARTrackedImageManager>();
    }

    void OnEnable()
    {
        _arTrackedImageManager.trackedImagesChanged += OnTrackedImagesChanged;
    }

    void OnDisable()
    {
        _arTrackedImageManager.trackedImagesChanged -= OnTrackedImagesChanged;
    }

    void OnTrackedImagesChanged(ARTrackedImagesChangedEventArgs eventArgs)
    {
        foreach (var trackedImage in eventArgs.added)
        {
            Handheld.Vibrate();
            PlayConfirmation();
            QRConfirmation();
            StartCoroutine(TimeDelay());
        }
    }

    public IEnumerator TimeDelay()
    {
        yield return new WaitForSeconds(1f);
        QRCodeARSO.SetActive(false);
        QRCodeUI.SetActive(false);
        MealMenuUI.SetActive(true);
        UIControl.SetActive(true);
        MainCamera.SetActive(true);
    }
}