﻿using UnityEngine;
using UnityEngine.UI;

public class AddSubtract : MonoBehaviour
{
    public int quantity;
    private int newQuantity;
    public Text displayQuantity;

    private void Start()
    {
        quantity = 0;
        displayQuantity.text = "0";
    }

    public void AddQuantity()
    {
        newQuantity = quantity++;
        displayQuantity.text = newQuantity.ToString();
    }

    public void SubtractQuantity()
    {
        newQuantity = quantity--;
        displayQuantity.text = newQuantity.ToString();
    }
}
