﻿using UnityEngine;
using UnityEngine.UI;


public class EnableUI : MonoBehaviour
{

    public GameObject quantityUI;
    public Text cartQuantity;
    public int quantity;
    private int newQuantity;
    public Text displayQuantity;

    private void Start()
    {
        quantity = 0;
        displayQuantity.text = "0";
    }

    public void ShowItemQuantity()
    {
        quantityUI.GetComponent<Animator>().Play("ShowQuantityUI");
    }

    public void AddToCart()
    {
        quantityUI.GetComponent<Animator>().Play("HideQuantityUI");
        cartQuantity.text = displayQuantity.text;
    }

    public void Cancel()
    {
        quantityUI.GetComponent<Animator>().Play("HideQuantityUI");
    }

    public void AddQuantity()
    {
        newQuantity = quantity++;
        displayQuantity.text = newQuantity.ToString();
    }

    public void SubtractQuantity()
    {
        newQuantity = quantity--;
        displayQuantity.text = newQuantity.ToString();
    }
}
