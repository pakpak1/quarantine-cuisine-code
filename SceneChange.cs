﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneChange : MonoBehaviour
{
    public void scanQR()
    {
        SceneManager.LoadScene("QR Code Scan (AR)");
    }

    public void loadAR1()
    {
        SceneManager.LoadScene("AR Dish 1");
    }

    public void loadAR2()
    {
        SceneManager.LoadScene("AR Dish 2");
    }

    public void loadAR3()
    {
        SceneManager.LoadScene("AR Dish 3");
    }

    public void loadAR4()
    {
        SceneManager.LoadScene("AR Dish 4");
    }

    public void loadAR5()
    {
        SceneManager.LoadScene("AR Dish 5");
    }

    public void mealMenu()
    {
        SceneManager.LoadScene("Meal Menu");
    }

    public void quarantineCuisine()
    {
        SceneManager.LoadScene("Quarantine Cuisine");
    }
}
