﻿using UnityEngine;
using UnityEngine.UI;

public class SwipeMenu1 : MonoBehaviour
{
    public GameObject scrollbar;                                    // Horizontal scrollbar game object for for swipe function
    public GameObject[] mealModels;                                 // Array of 3d models to spawn
    public ARTapToPlaceObject arTapToPlaceObjectScript;             // Allow reference to arTapToPlaceObject script
    private GameObject spawnedObject;                               // The spawned meal model
    private float scroll_pos = 0;                                   // Reference to the scroll position
    float[] pos;                                                    // An array for set meal menu positions
    private int old_item = 0;                                       // Set counter for old_item to 0
    private int objectIndex = 0;                                    // Set counter for objectIndex to 0
    public Vector3 spawnedObjectLocation;                           // Variable to store spawned object location from arTapToPlaceObject script


    void Start()
    {
        // store PlayerController script located on a different game object in a variable to be used in this script
        arTapToPlaceObjectScript = GameObject.Find("Plane Detection ARSO").GetComponent<ARTapToPlaceObject>();
    }

    // Update is called once per frame
    void Update()
    {
        // Set position array equal to the number of children under parent object
        // Distance variable set to split into a fraction dependent on the number of children under parent object
        // As user scrolls, position changes based on the child count

        pos = new float[transform.childCount];
        float distance = 1f / (pos.Length - 1f);
        for (int i = 0; i < pos.Length; i++)
        {
            pos[i] = distance * i;
        }

        // gets position of scroll bar on touch
        if (Input.GetMouseButton(0))
        {
            scroll_pos = scrollbar.GetComponent<Scrollbar>().value;
        }
        else
        {
            for (int i = 0; i < pos.Length; i++)
            {
                // if the current scroll bar position is less than the position of child object plus 1/2 it's length and the scroll bar
                // position is greater than the position of the child object minus 1/2 it's length, use lerp to smoothly transition between child positions

                if (scroll_pos < pos[i] + (distance / 2) && scroll_pos > pos[i] - (distance / 2))
                {
                    scrollbar.GetComponent<Scrollbar>().value = Mathf.Lerp(scrollbar.GetComponent<Scrollbar>().value, pos[i], 0.1f);
                    objectIndex++;
                }
            }
        }


        for (int i = 0; i < pos.Length; i++)
        {
            if (scroll_pos < pos[i] + (distance / 2) && scroll_pos > pos[i] - (distance / 2))
            {
                Debug.LogWarning("Current Selected Level" + i);
                transform.GetChild(i).localScale = Vector2.Lerp(transform.GetChild(i).localScale, new Vector2(1f, 1f), 0.1f);

                // Collect spawned object from ARTapToPlaceObject script
                // Collect spawned object location
                spawnedObject = arTapToPlaceObjectScript.spawnedObject;
                spawnedObjectLocation = spawnedObject.transform.position;
                Destroy(arTapToPlaceObjectScript);

                for (int j = 0; j < pos.Length; j++)
                {
                    if (j < i)
                    {
                        transform.GetChild(j).localScale = Vector2.Lerp(transform.GetChild(j).localScale, new Vector2(0.8f, 0.8f), 0.1f);
                        objectIndex--;
                        ModelSpawn();
                    }
                    else if (j > i)
                    {
                        transform.GetChild(j).localScale = Vector2.Lerp(transform.GetChild(j).localScale, new Vector2(0.8f, 0.8f), 0.1f);
                        objectIndex++;
                        ModelSpawn();
                    }

                }


            }

        }

    }

    public void ModelSpawn()
    {
        // When user swipes, old_item shouldn't equal i, so destroy the spawned object and instantiate the new one at spawnedObjectLocation
        // Destroy arTapToPlaceObjectScript so no more taps are registered
        // if (old_item != objectIndex)
        // {

        Destroy(spawnedObject);
        Instantiate(mealModels[objectIndex], spawnedObjectLocation, transform.rotation);
        spawnedObject = mealModels[objectIndex];

        if (objectIndex > old_item)
        {
            old_item++;
        }
        else if (objectIndex < old_item)
        {
            old_item--;
        }
    }
}

// Problem: we need a way to track whether you swipe right or left. Currently, int i only counts up.
// Need a way for variable to track whether we swiped right or left, and then pass these changes to the meal model index

