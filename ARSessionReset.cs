﻿using UnityEngine;
using UnityEngine.XR.ARFoundation;

public class ARSessionReset : MonoBehaviour
{
    void Awake()
    {
        ARSession arSession = GetComponent<ARSession>();
        arSession.Reset();
    }
}
