﻿using UnityEngine;

public class ARSessionManager : MonoBehaviour
{
    public GameObject QRCodeARSO;
    public GameObject QRCodeUI;
    public GameObject MealMenuUI;
    public GameObject UIControl;
    public GameObject PlaneDetectionARSO;
    public GameObject PlaneDetectionUI;
    public GameObject MainCamera;

    void Awake()
    {
        MainCamera.SetActive (false);
        QRCodeARSO.SetActive (true);
        QRCodeUI.SetActive (true);
        MealMenuUI.SetActive (false);
        UIControl.SetActive (false);
        PlaneDetectionARSO.SetActive (false);
        PlaneDetectionUI.SetActive (false);
    }

    public void PlaneDetection()
    {
        MainCamera.SetActive(false);
        QRCodeARSO.SetActive(false);
        QRCodeUI.SetActive(false);
        MealMenuUI.SetActive(false);
        UIControl.SetActive(false);
        PlaneDetectionARSO.SetActive(true);
        PlaneDetectionUI.SetActive(true);
    }

}
