﻿using UnityEngine;

public class ObjectRotation : MonoBehaviour
{
    public float turnSpeed = 25f;

    void Update()
    {
        ObjectRotate();
    }
    public void ObjectRotate()
    {
        transform.Rotate(Vector3.up, turnSpeed * Time.deltaTime);
    }

}
